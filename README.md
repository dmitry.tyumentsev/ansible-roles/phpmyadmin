phpMyAdmin Role
=========

Thiss role installs [phpMyAdmin](https://www.phpmyadmin.net/) on Ubuntu/Debian server.

Example Playbook
=========
```yml
- hosts: all
  become: true
  roles:
    - phpmyadmin
```
